#!/bin/bash

clean_elk() {
    #Installing wget.
    sudo yum install wget bind-utils curl -y

echo -e "\n"
echo "*****************************************************************************"
echo "               Cleaning up the ELK and Nginx from server.               "
echo "*****************************************************************************"
echo -e "\n"

	for i in elasticsearch logstash kibana
			do
				rpm -q $i
				if [ $? == 0 ]
					then 
                        echo "Cleaning ELK from system"
                        sudo systemctl disable elasticsearch logstash kibana && sudo systemctl stop elasticsearch logstash kibana > /dev/null 2>&1
                        sudo yum remove -y elasticsearch logstash kibana nginx > /dev/null 2>&1
                        sudo rm -rf /etc/pki/tls/certs/logstash-forwarder.crt
                        sudo rm -rf /etc/pki/tls/private/logstash-forwarder.key
                        sudo rm -rf  /etc/elasticsearch/ /etc/logstash/ /etc/kibana/                       
					else
						echo "ELK NOT installed."
				fi
			done

}

echo -e "\n"
echo "*****************************************************************************"
echo "               ELK has been removed from this system               "
echo "*****************************************************************************"
echo -e "\n"

clean_elk
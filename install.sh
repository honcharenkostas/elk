#!/bin/bash

# Checking user permission to run this script
sudo -n true
if [ $? -ne 0 ]
    then
        echo "This script requires users to have passwordless sudo access"
        exit
fi



echo -e "\n"
echo "*****************************************************************************"
echo -e "\n"

sudo yum update -y
sudo yum install bind-utils net-tools unzip wget curl -y


# Gather Information.
# Get Pubic ip of the server.
GLOBAL_RESOLVED_IP=$(dig +short myip.opendns.com @resolver1.opendns.com)
PRIVATE_IP=$(ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p')

read -p "Enter the Public ip address for this server. we found $GLOBAL_RESOLVED_IP. ENTER IT AGAIN :  "  PUBG_IP


echo -e "\n"
echo "*****************************************************************************"
echo -e "\n"

#read -p "Ener the Private ip address for this server.\n\n It will be used to make connection between logstash and filebeat on port 5046 :  "  PRIVG_IP


echo -e "\n"
echo "*****************************************************************************"
echo -e "\n"

read -p "Authentication username to access Kibana: for e.g admin :  " AUTH_USER

echo -e "\n"
echo "*****************************************************************************"
echo -e "\n"

read -s -p "Please create a password for user $AUTH_USER :  "  AUTH_PASSWD


echo -e "\n"
echo "*****************************************************************************"
echo -e "\n"


echo "Checking Java version on this system"


install_java() {
    java -version
    if [ $? -ne 0 ]
        then
            sudo yum install jre-1.8.0-openjdk -y
    elif [ "`java -version 2> /tmp/version && awk '/version/ { gsub(/"/, "", $NF); print ( $NF < 1.8 ) ? "YES" : "NO" }' /tmp/version`" == "YES" ]
        then
            sudo yum install jre-1.8.0-openjdk -y
    fi
}

echo -e "\n"
echo "*****************************************************************************"
echo "                    Setting up ELK on this system.               "
echo "*****************************************************************************"
echo -e "\n"

install_elk() {
    #Installing wget.
    sudo yum install wget bind-utils curl -y

	for i in elasticsearch logstash kibana
			do
				rpm -q $i
				if [ ! $? == 0 ]
					then 
						sudo yum localinstall elasticsearch-6.4.2.rpm -y		
						sudo yum localinstall logstash-6.4.2.rpm -y	
						sudo yum localinstall kibana-6.4.2-x86_64.rpm -y									
					else
						echo "ELK already installed."
				fi
			done

}

echo -e "\n"
echo "*****************************************************************************"
echo "                    Enabling the services.               "
echo "*****************************************************************************"
echo -e "\n"

	sudo systemctl enable elasticsearch logstash kibana 



# Copying files and certificates to their location.
copy_files() {
	yes | cp elasticsearch.yml /etc/elasticsearch/
	yes | cp pipeline.yml /etc/logstash/pipelines.yml
    yes | cp logstash-json.conf /etc/logstash/conf.d/
	yes | cp kibana.yml /etc/kibana/
#	yes | cp logstash-forwarder.crt /etc/pki/tls/certs/logstash-forwarder.crt
#	yes | cp logstash-forwarder.key /etc/pki/tls/private/logstash-forwarder.key
}
# Create htpasswd for authentication.
echo -e "\n"
echo "*****************************************************************************"
echo "       Setting up Security for Kibana with User based authentication.        "
echo "*****************************************************************************"
echo -e "\n"


create_htpasswd() {
	# Check if file already exists.
	if [ -f /etc/nginx/htpasswd.users ] 
	then
		echo "File already exists."
		htpasswd -b /etc/nginx/htpasswd.users $AUTH_USER $AUTH_PASSWD
	else
		echo "File does not exist. Creating new file to store passwd."
		htpasswd -b -c /etc/nginx/htpasswd.users $AUTH_USER $AUTH_PASSWD	
	fi	
}

# Setting up proxy for Kibana with Nginx.
echo -e "\n"
echo "*****************************************************************************"
echo "               Setting up Reverse proxy for Kibana with Nginx.               "
echo "*****************************************************************************"
echo -e "\n"

install_nginx() {
    # nginx
	rpm -q nginx
	if [ $? == 0 ]
    then
        echo "nginx installed"
    else
        yum -y install epel-release
        yum -y install nginx httpd-tools
	sed -i "47d;48d" /etc/nginx/nginx.conf
        mkdir /etc/nginx/conf.d/
        sudo cp nginx.conf /etc/nginx/conf.d/kibana.conf
		sed -i "s/PUBG_IP/${PUBG_IP}/g" /etc/nginx/conf.d/kibana.conf
		nginx -t > /dev/null 2>&1
		if [ ! $? == 0 ]
		then
			echo "nginx configuration failed"
			exit
		fi
        sudo systemctl start nginx
        sudo systemctl enable nginx
    fi
}

# Generate SSL key and cert for logstash.
ssl_gen() {
yes | cp openssl.cnf /etc/pki/tls/openssl.cnf
cd /etc/pki/tls
sed -i "s/PUBG_IP/${PRIVATE_IP}/g" /etc/pki/tls/openssl.cnf
sudo openssl req -config /etc/pki/tls/openssl.cnf -x509 -days 3650 -batch -nodes -newkey rsa:2048 -keyout private/logstash-forwarder.key -out certs/logstash-forwarder.crt
cat /etc/pki/tls/certs/logstash-forwarder.crt > logstash-forwarder.crt
}

run_elk() {
		sudo systemctl enable elasticsearch logstash kibana
		sudo systemctl daemon-reload		
		sudo systemctl start elasticsearch logstash kibana
}

firewall_cmd() {
	firewall-cmd --add-port={7300,6200,9600,9300,5046,80,443}/tcp --permanent
	firewall-cmd --reload
	setenforce 0
}

create_htpasswd $AUTH_USER $AUTH_PASSWD

install_nginx
install_java
install_elk
copy_files
ssl_gen
run_elk
create_htpasswd $AUTH_USER $AUTH_PASSWD

# ELK Server

![Scheme](img/elk.gif)

### Installation guide:
1) Clone app to to ELK server 
2) Install ELK stack (Elasticsearch, Logstash, Kibana, Nginx, SSL cert configuration, open ports)

```sudo sh install.sh```

*You will be asked to set up server IP, Kibana login/password via installation process*

3) Import Kibana dashboard configuration    
    - Go to Kibana home page
    - Click "Management" tab
    - Click "Saved object" link
    - Click "Import" link & import fil kibana-dashboard-config.json
    
### Un-instalation guide 
```sudo sh uninstall.sh```
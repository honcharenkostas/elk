#!/bin/bash

# Checking user permission to run this script
sudo -n true
if [ $? -ne 0 ]
    then
        echo "This script requires users to have passwordless sudo access"
        exit
fi

echo -e "\n"
echo "*****************************************************************************"
echo "               Setting up Fileeat server.               "
echo "*****************************************************************************"
echo -e "\n\n"


read -p "Enter the Private IP address of Logstash server and make sure 5046 port is available to this server. " LOGSTASH_SVR_IP

echo -e "\n"
echo "***************************************************************************************************************"
echo "               Checking if Logstash server is accessible on Port 5046 from server $LOGSTASH_SVR_IP.             "
echo "***************************************************************************************************************"
echo -e "\n\n"

timeout 1 bash -c "</dev/tcp/$LOGSTASH_SVR_IP/5046"
if [ ! $? == 0 ]
then 
	echo "Can't connect to the port 5046 on server $LOGSTASH_SVR_IP. Please make sure the Port is accessible from this server for this to work." 
	exit
else
	echo "Port 5046 is open on server $LOGSTASH_SVR_IP"	
fi 
echo -e "\n\n"
echo "***************************************************************************************************************"
echo -e "\n\n"


install_filebeat() {
    #Installing wget.
    sudo yum install wget -y

echo -e "Please make sure to copy the logstash-forwarder.crt from logstash server to this server. \n\n Place the cert /etc/ssl/certs/logstash-forwarder.crt after this script is complete. \n\n Restart the filebeat server. "

	for i in filebeat
			do
				rpm -q $i
				if [ ! $? == 0 ]
					then 
						sudo wget  https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-6.4.2-x86_64.rpm			
						sudo yum localinstall filebeat-6.4.2-x86_64.rpm -y									
					else
						echo "ELK already installed."
				fi
			done

}

copy_files() {
	yes | cp filebeat.yml /etc/filebeat/filebeat.yml
	sed -i "s/LOGSTASH_IP/${LOGSTASH_SVR_IP}/g" /etc/filebeat/filebeat.yml
	yes | cp logstash-forwarder.crt /etc/ssl/certs/logstash-forwarder.crt
}

start_filebeat() {
	sudo systemctl restart filebeat && systemctl enable filebeat

}


create_htpasswd $AUTH_USER $AUTH_PASSWD

install_filebeat
copy_files
start_filebeat

